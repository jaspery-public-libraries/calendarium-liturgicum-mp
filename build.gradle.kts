/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.ext.dependencies.assertk
import com.jaspery.gradle.ext.dependencies.junit
import com.jaspery.gradle.ext.dependencies.jvmTestImplementation
import com.jaspery.gradle.ext.kotlin.`kotlin-multiplatform`

plugins {
    `kotlin-multiplatform`
}

kotlin { jvm() }

allprojects {
    apply { from("$rootDir/_repositories.gradle.kts") }
    apply { plugin("maven-publish") }
}


subprojects {
    apply { plugin(`kotlin-multiplatform`) }

    kotlin {
        jvm()
        // Dependency Management
        dependencies {
            val versions = object {
                // dependencies
                val kotlinVersion: String by project
                val assertkVersion: String by project
                val jkeJklMpVersion: String by project
                val junitVersion: String by project
            }
            @Suppress("UnstableApiUsage")
            constraints {
                commonMainImplementation(kotlin("stdlib-jdk8", versions.kotlinVersion))
                commonMainImplementation(jpl("jaspery-kotlin-ext-mp:jaspery-kotlin-lang-mp", versions.jkeJklMpVersion))

                commonTestImplementation(assertk(version = versions.assertkVersion))

                jvmTestImplementation(junit(versions.junitVersion))
            }
        }
    }
}

fun DependencyHandlerScope.jpl(simpleModuleName: String, version: String? = null): String =
        "com.jaspery.jaspery-public-libraries.$simpleModuleName" + version?.let { ":$it" }.orEmpty()
