/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

repositories {
    mavenCentral {
        mavenContent {
            includeGroupByRegex("org\\.jetbrains.*")

            approvedTransitiveDependenciesOfKotlinGradlePlugin()
        }
    }

    maven("https://gitlab.com/api/v4/projects/18935877/packages/maven") {
        name = "GitLab::m2-repo::JPL"
        mavenContent {
            includeModule("com.jaspery.jaspery-public-libraries", "jpl-dependencies-platform")
            includeModule("com.jaspery.jaspery-public-libraries.jaspery-kotlin-build-utils", "jaspery-kotlin-mp-build")
        }
    }
}

fun MavenRepositoryContentDescriptor.approvedTransitiveDependenciesOfKotlinGradlePlugin() {
    includeModule("de.undercouch", "gradle-download-task")
    includeModuleByRegex("com\\.google\\.code\\.gson", "gson(-parent)*")
    includeModule("org.sonatype.oss", "oss-parent")
    includeModule("com.github.gundy", "semver4j")
    includeModuleByRegex("org\\.antlr", "antlr4-(runtime|master)")
}
