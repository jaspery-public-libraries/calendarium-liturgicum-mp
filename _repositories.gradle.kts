/*
 * Copyright © 2021. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

repositories {
    mavenCentral() {
        @Suppress("UnstableApiUsage")
        mavenContent {
            includeGroupByRegex("org\\.jetbrains.*")
            includeGroup("com.willowtreeapps.assertk")
            includeGroup("com.willowtreeapps.opentest4k")
            includeModule("org.opentest4j", "opentest4j")
            includeModule("org.threeten", "threetenbp")
            includeModule("junit", "junit")
            includeGroup("org.hamcrest")
        }
    }
    maven("https://gitlab.com/api/v4/projects/18935877/packages/maven") {
        name = "GitLab::m2-repo::JPL"
        mavenContent {
            includeModule("com.jaspery.jaspery-public-libraries.jaspery-kotlin-ext-mp", "jaspery-kotlin-lang-mp")
            includeModule("com.jaspery.jaspery-public-libraries.jaspery-kotlin-ext-mp", "jaspery-kotlin-lang-mp-js")
            includeModule("com.jaspery.jaspery-public-libraries.jaspery-kotlin-ext-mp", "jaspery-kotlin-lang-mp-jvm")
            includeModule("com.jaspery.jaspery-public-libraries.jaspery-kotlin-ext-mp", "jaspery-kotlin-lang-mp-metadata")
        }
    }
}

fun MavenRepositoryContentDescriptor.jpl(simpleModuleName: String, version: String? = null): String =
        "com.gitlab.jaspery-public-libraries.$simpleModuleName" + version?.let { ":$it" }.orEmpty()

