/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */
rootProject.name = "calendarium-liturgicum-mp"
include("easter-computus-mp")

pluginManagement {
    @Suppress("UnstableApiUsage")
    plugins {
        // load versions from gradle.properties
        val kotlinVersion: String by settings
        // declare default plugin versions
        kotlin("multiplatform") version kotlinVersion
    }
}
