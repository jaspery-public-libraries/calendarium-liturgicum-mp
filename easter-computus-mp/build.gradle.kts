/*
 * Copyright © 2020. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

import com.jaspery.gradle.ext.dependencies.assertk
import com.jaspery.gradle.ext.dependencies.jpl
import com.jaspery.gradle.ext.kotlin.*

val jitpack = System.getenv("JITPACK")?.toBoolean() ?: false

plugins {
    kotlin("multiplatform") apply true
}

kotlin {
    jvm { compilations.all { kotlinOptions { jvmTarget = "1.8" } } }
    js {
        nodejs()
        browser {
            testTask {
                enabled = !jitpack
                useKarma {
                    useChromeHeadless()
                }
            }
        }
    }

    sourceSets {
        commonMainDependencies {
            implementation(kotlin("stdlib-common"))
            implementation("com.jaspery.jaspery-public-libraries.jaspery-kotlin-ext-mp:jaspery-kotlin-lang-mp")
        }

        commonTestDependencies {
            implementation(kotlin("test-common"))
            implementation(kotlin("test-annotations-common"))
            implementation(assertk())
        }

        jvmMainDependencies {
            implementation(kotlin("stdlib"))
        }

        jvmTestDependencies {
            implementation(kotlin("test-junit"))
            implementation(kotlin("reflect"))
        }

        jsMainDependencies {
            implementation(kotlin("stdlib-js"))
        }

        jsTestDependencies {
            implementation(kotlin("test-js"))
        }
    }
}
