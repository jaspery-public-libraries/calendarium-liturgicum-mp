/*
 * Copyright © 2019. Ihor Halanyuk https://gitlab.com/jaspery-public-libraries
 */

package com.jaspery.calendarium.computus.impl.gauss

import assertk.all
import assertk.assertThat
import assertk.assertions.*
import com.jaspery.kotlin.date.date
import com.jaspery.kotlin.date.year
import kotlin.js.JsName
import kotlin.test.Test

class GaussEasterComputusTest {
    private val easterComputus: GaussEasterComputus = GaussEasterComputus()

    @Test
    @JsName("testComputeEasterDateFor_2018_Gregorian")
    fun `test compute Easter date for 2018 Gregorian`() {
        assertThat(easterComputus.compute(2018.year)).isEqualTo(EASTER_2018.second)
    }

    @Test
    @JsName("testComputeEasterDateFor_2018_Julian_AprilBranch")
    fun `test compute Easter date for 2018 Julian, April branch`() {
        assertThat(easterComputus.computeJulian(2018.year)).isEqualTo(EASTER_2018.third)
    }

    @Test
    @JsName("testComputeEasterDateFor_2016_Julian_MayBranch")
    fun `test compute Easter date for 2016 Julian, May branch`() {
        val actual = easterComputus.computeJulian(2016.year)
        assertThat(actual).isEqualTo(EASTER_2016.third)
    }

    @Test
    @JsName("test_Gregorian_Julian_conversion")
    fun `test Gregorian-Julian conversion`() {
        assertThat(easterComputus.julianCalendarShift(2018)).isEqualTo(13)
    }

    @Test
    @JsName("test_compute_Easter_date_before_algorithm_valid")
    fun `test compute Easter date before algorithm valid`() {
        assertThat {
            easterComputus.compute(1582.year)
        }.isFailure().all {
            isInstanceOf(IllegalArgumentException::class)
            message().isNotNull().startsWith("Cannot calculate easter date before 1583, but ")
        }
    }

    companion object {
        private val EASTER_2018 = Triple(2018.year, "2018-04-01".date, "2018-04-08".date)
        private val EASTER_2016 = Triple(2016.year, "2016-03-27".date, "2016-05-01".date)
    }
}
